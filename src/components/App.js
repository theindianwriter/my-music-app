import React, { Component } from 'react';
import Searchbox from './Searchbox';
import Filterbox from './Filterbox';
import Musiccard from './Musiccard';



class App extends Component {
  
  render() {
    return (
      <div className="container-fluid">
       <Searchbox />
       <div className="row">
       <Filterbox />
       <Musiccard />
       </div>
      </div>
    );
  }
}

export default App;
