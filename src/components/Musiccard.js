import React from 'react';
import { connect } from 'react-redux';
import songsList from '../selectors/musicSelector'
export class Musiccard extends React.Component{
    getMusicList = () =>{
         return(
         this.props.musicList.map(music=>(
          <div key={music.name} className="well " style={{display: "inline-block",height: "20rem", width: "25rem",margin: "5px",backgroundColor: "#f38980"}} >
                  <h4>Song: <small><mark>{music.name}</mark></small></h4>
                  <h4>Artist: <small><mark>{music.artist}</mark></small></h4>  
                  <h4>Duration: <small>{music.length}</small></h4>
                  <h4>Genre: <small>{music.genre}</small></h4>
                    
            </div>
         ))
         );
    }
    render(){ 
        return(
            <div className="col-sm-11">
                <div className="container" style={{margin: "15px"}}>
                    {this.getMusicList()}
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
   return {musicList: songsList(state)};
}


export default connect(
    mapStateToProps
)(Musiccard);
  
