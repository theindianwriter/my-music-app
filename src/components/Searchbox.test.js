import React from "react";
import Enzyme, { shallow } from "enzyme";
import { Searchbox } from "./Searchbox";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

describe("test on searchbox", () => {
  test("the search box is rendered correctly", () => {
    const wrapper = shallow(<Searchbox />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.debug()).toMatchSnapshot();
  });

  test("onchange is fired when something is typed", () => {
    const mockOnChange = jest.fn();
    let event = {
      target: {
        value: "hello"
      }
    };
    const wrapper = shallow(<Searchbox handleOnChange={mockOnChange} />);
    wrapper.find("input").simulate("change", event);
    expect(mockOnChange).toHaveBeenCalledWith("hello");
  });
});
