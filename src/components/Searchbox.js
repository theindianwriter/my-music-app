import React from "react";
import { connect } from "react-redux";

export class Searchbox extends React.Component {
  handleOnChange = (e)=>{
    this.props.handleOnChange(e.target.value)
  }
  render() {
    
    return (
      <div style={{ margin: "15px" }}>
        <input
          type="text"
          id="searchbox"
          className="form-control"
          placeholder="Search for Name or Artist"
          value={this.props.value}
          onChange={this.handleOnChange}
        />
        
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { value: state.value};
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleOnChange: (value) => {
      return dispatch({
        type: "CHANGE_SEARCH",
        payload: value,
      });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Searchbox);
