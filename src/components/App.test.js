import React from "react";
import Enzyme, { shallow } from "enzyme";
import App from "./App";
import Adapter from "enzyme-adapter-react-16";
import Searchbox from "./Searchbox";
import Musiccard from "./Musiccard";
import Filterbox from "./Filterbox";

Enzyme.configure({ adapter: new Adapter() });

describe(" test for music app", () => {
  const wrapper = shallow(<App />);
  test("just a test for App rendering", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
  test("to test if Searchbox is rendered once", () => {
    expect(wrapper.find(Searchbox)).toHaveLength(1);
  });
  test("to test if Musiccard is rendered once", () => {
    expect(wrapper.find(Musiccard)).toHaveLength(1);
  });
  test("to test if Filterbox is rendered once", () => {
    expect(wrapper.find(Filterbox)).toHaveLength(1);
  });
});
