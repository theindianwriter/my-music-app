import React from "react";
import Enzyme, { shallow } from "enzyme";
import { Filterbox } from "./Filterbox";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

describe("test on filterbox", () => {
  let initialProps;
  beforeEach(() => {
    initialProps = {
      genres: ["romance", "bollywood"],
      artists: ["priyam guha", "ellie goulding"]
    };
  });
  afterEach(() => {
    initialProps = {};
  });
  test("it should  rendered correctly ", () => {
    const wrapper = shallow(<Filterbox filterboxList={initialProps} />);
    expect(wrapper.debug()).toMatchSnapshot();
  });

  test("onchange is fired when romance is selected", () => {
    const mockOnChange = jest.fn();
    let event = {
      target: {
        value: "romance",
        checked: true
      }
    };

    const wrapper = shallow(
      <Filterbox onChange={mockOnChange} filterboxList={initialProps} />
    );
    wrapper.find("#romance").simulate("change", event);
    expect(mockOnChange).toHaveBeenCalledWith("romance", true, "genre");
  });
});
