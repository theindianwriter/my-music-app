import React from "react";
import Enzyme, { shallow, mount } from "enzyme";
import { Musiccard } from "./Musiccard";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

describe("test for musiccard", () => {
  let musicList;
  beforeEach(() => {
    musicList = [
      {
        id: 1,
        name: "love me like you do",
        artist: "ellie goulding",
        length: "4:09",
        genre: "romance"
      },
      {
        id: 2,
        name: "i know you",
        artist: "skylar grey",
        length: "4:59",
        genre: "romance"
      }
    ];
  });

  afterEach(()=>{
    musicList = [];
  })
  test("just a test for musiccard rendering", () => {
    const wrapper = shallow(<Musiccard musicList={musicList} />);
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
