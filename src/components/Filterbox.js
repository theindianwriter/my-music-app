import React from "react";
import { connect } from "react-redux";
import filterlist from "../selectors/filterbox_selector";

export class Filterbox extends React.Component {
  getFilterBox(songTags, parameter) {
    return songTags.map(songTag => {
      return (
        <span key={songTag}>
          <input
            id={songTag}
            type="checkbox"
            value={songTag}
            onChange={e =>
              this.props.onChange(e.target.value, e.target.checked, parameter)
            }
          />
          {songTag}
          <br />
        </span>
      );
    });
  }

  render() {
    return (
      <div className="col-sm-1">
        <div>
          <h4>Filter by Genre</h4>
          {this.getFilterBox(this.props.filterboxList.genres, "genre")}
        </div>
        <div>
          <h4>Filter by artist</h4>
          {this.getFilterBox(this.props.filterboxList.artists, "artist")}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    filterboxList: filterlist(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChange: (value, status, parameter) => {
      dispatch({
        type: "FILTER",
        value: value,
        status: status,
        parameter: parameter
      });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filterbox);
