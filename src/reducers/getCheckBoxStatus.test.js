import getCheckboxesStatus from "./getCheckBoxesStatus";

describe("test for checkbox reducer", () => {
  let state;
  beforeEach(() => {
    state = { genres: [], artists: [] };
  });
  test("it should return the default state", () => {
    expect(getCheckboxesStatus(undefined, {})).toEqual(state);
  });
  test("it should return correct state for genre", () => {
    let action = {
      type: "FILTER",
      parameter: "genre",
      status: true,
      value: "romance"
    };
    state.genres = ["romance"];
    expect(getCheckboxesStatus(undefined, action)).toEqual(state);
  });
  test("it should return correct state for genre and artist", () => {
    let action = {
      type: "FILTER",
      parameter: "artist",
      status: true,
      value: "priyam guha"
    };
    state.genres = ["romance"];
    state.artists = ["skylar grey","priyam guha"];
    expect(
      getCheckboxesStatus({ artists: ["skylar grey"], genres: ["romance"] }, action)
    ).toEqual(state);
  });
  test("it should return the correct state for unchecking the genre checkbox", () => {
    let action = {
      type: "FILTER",
      parameter: "genre",
      status: false,
      value: "romance"
    };
    state.artists = ["priyam guha"];
    state.genres = [];
    expect(
      getCheckboxesStatus(
        { artists: ["priyam guha"], genres: ["romance"] },
        action
      )
    ).toEqual(state);
  });
});
