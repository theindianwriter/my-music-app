let initialState = [
    {
      id : 1,
      name: "love me like you do",
      artist: "ellie goulding",
      length: "4:09",
      genre: "romance",
    },
    {
      id : 2,
      name: "i know you",
      artist: "skylar grey",
      length: "4:59",
      genre: "romance",
  
    },
    {
      id : 3,
      name: "skrt on me",
      artist: "calvin harris",
      length: "3:08",
      genre: "metal",
    
    },
    {
      id : 4,
      name: "side",
      artist: "khalid",
      length: "5:08",
      genre: "rock",
    
    },
    {
      id : 4,
      name: "cash out",
      artist: "schoolboy",
      length: "3:45",
      genre: "funk",
    
    },
    {
      id : 5,
      name: "hard to love",
      artist: "jessie reyez",
      length: "4:59",
      genre: "jazz",
    
    },
    {
      id : 6,
      name: "akiya hari darshan",
      artist: "pandit bhimsen joshi",
      length: "7:56",
      genre: "classical",
   
    },
    {
      id : 7,
      name: "baaje re murila baje",
      artist: "lata",
      length: "4:59",
      genre: "sad",
    },
    {
      id : 8,
      name: "maine pyaar kyu kiya",
      artist: "shirley shetty",
      length: "4:56",
      genre: "bollywood",
    
    },
    {
      id : 9,
      name: "sri ram chandra kripa",
      artist: "chhannulal mishra",
      length: "9:08",
      genre: "classical",
      
    },
    {
      id : 10,
      name: "hari kripa",
      artist: "priyam guha",
      length: "3:33",
      genre: "jazz",
    
    },
    {
      id : 11,
      name: "panihari",
      artist: "anupriya lakhawat",
      length: "9:09",
      genre: "folk",
   
    },
    {
      id : 12,
      name: "kasam se",
      artist: "ellie goulding",
      length: "4:09",
      genre: "romance",
      
    },
    {
      id : 13,
      name: "kal ho na ho",
      artist: "skylar grey",
      length: "4:59",
      genre: "romance",
      
    },
    {
      id : 14,
      name: "nit khair manga",
      artist: "calvin harris",
      length: "3:08",
      genre: "metal"
    },
    {
      id : 15,
      name: "maa tujhe salam",
      artist: "khalid",
      length: "5:08",
      genre: "rock",
      
    },
    {
      id : 16,
      name: "vande mataram",
      artist: "schoolboy",
      length: "3:45",
      genre: "funk",
      
    },
    {
      id : 17,
      name: "joker",
      artist: "jessie reyez",
      length: "4:59",
      genre: "rock",
      
    },
    {
      id : 18,
      name: "haale dil",
      artist: "pandit bhimsen joshi",
      length: "7:56",
      genre: "sad",
      
    },
    {
      id : 19,
      name: "halka halka",
      artist: "lata",
      length: "4:59",
      genre: "pop",
    
    },
    {
      id : 20,
      name: "tum ho to",
      artist: "shirley shetty",
      length: "4:56",
      genre: "bollywood"
    },
    {
      id : 21,
      name: "kajra re",
      artist: "chhannulal mishra",
      length: "9:08",
      genre: "classical",
    },
    {
      id : 22,
      name: "hari darshan",
      artist: "priyam guha",
      length: "3:33",
      genre: "pop"
    },
    {
      id : 23,
      name: "panwala",
      artist: "anupriya lakhawat",
      length: "9:09",
      genre: "folk",
    }
  ];

  const getAllSongs = ()=>{
      return initialState;
  }

  export default getAllSongs;