export default function searchBoxValue(state = "", action="") {
  switch (action.type) {
    case "CHANGE_SEARCH":
      return action.payload;
    default:
      return state;
  }
}
