import searchBoxValue from "./searchBoxValue";

describe("test for search box reducer", () => {
  test("it should return correct state", () => {
    expect(
      searchBoxValue("", { type: "CHANGE_SEARCH", payload: "hello" })
    ).toEqual("hello");
  });
});
