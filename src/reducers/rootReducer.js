import { combineReducers } from "redux";
import searchBoxValue from "./searchBoxValue";
import getCheckBoxesStatus from "./getCheckBoxesStatus";
import getAllSongs from './getAllSongs';

const rootReducer = combineReducers({
  allsongs : getAllSongs,
  value: searchBoxValue,
  CheckBoxes: getCheckBoxesStatus
});

export default rootReducer;
