export default function getCheckBoxesStatus(
  state = { genres: [], artists: [] },
  action=""
) {
  switch (action.type) {
    case "FILTER":
      let newState = { ...state };
      if (action.parameter === "genre") {
        newState.genres = action.status
          ? newState.genres.concat([action.value])
          : newState.genres.filter(value => value !== action.value);
      } else {
        newState.artists = action.status
        ? newState.artists.concat([action.value])
        : newState.artists.filter(value => value !== action.value);
      }
      return newState;
    default:
      return state;
  }
}
