import { createSelector } from "reselect";

const getAllSongs = state => state.allsongs;
const getcheckboxes = state => state.CheckBoxes;
const getSearchValue = state => state.value;

function filterSongsByCheckbox(val, toCheck) {
  return toCheck.length ? toCheck.includes(val) : true;
}

const songsList = createSelector(
  [getAllSongs, getcheckboxes, getSearchValue],
  (allsongs, Checkboxes, value) => {
    let payload = value.toLowerCase().trim();
    return allsongs.filter(song => {
      if (
        song.name.toLowerCase().indexOf(payload) === -1 &&
        song.artist.toLowerCase().indexOf(payload) === -1
      )
        return false;
      else {
        return (
          filterSongsByCheckbox(song.genre.toLowerCase(), Checkboxes.genres) &&
          filterSongsByCheckbox(song.artist.toLowerCase(), Checkboxes.artists)
        );
      }
    });
  }
);

export default songsList;
