import { createSelector } from "reselect";

const getallsongs = state => state.allsongs;

const filterlist = createSelector(
  getallsongs,
  songs => {
    const genres = [];
    const artists = [];
    songs.forEach(song => {
      if (!artists.includes(song.artist)) artists.push(song.artist);
      if (!genres.includes(song.genre)) genres.push(song.genre);
    });
    return { genres: genres, artists: artists };
  }
);

export default filterlist;
