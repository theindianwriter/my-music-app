import songsList from "./musicSelector";
import { testState }from './testStore'
describe("test for music selector", () => {
  let state;
  beforeEach(() => {
    state = JSON.parse(JSON.stringify(testState));
  });
  afterEach(() => {
    state = {};
  });
  test("test for the initital state", () => {
    let expectedState = state.allsongs;
    expect(songsList(state)).toEqual(expectedState);
  });
  test("it should return the correct state", () => {
    state.CheckBoxes.artists = ["priyam guha"];
    state.value = "kripa";
    let expectedState = [
      {
        id: 10,
        name: "hari kripa",
        artist: "priyam guha",
        length: "3:33",
        genre: "jazz"
      }
    ];
    expect(songsList(state)).toEqual(expectedState);
  });
  test("space in the beginning test", () => {
    state.value = "   ";
    let expectedState = state.allsongs;
    expect(songsList(state)).toEqual(expectedState);
  });
  test("alpha numeric test", () => {
    state.value = "@@123";
    let expectedState = [];
    expect(songsList(state)).toEqual(expectedState);
  });
  test("multiple checkboxes selected test", () => {
    state.CheckBoxes.artists = ["ellie goulding"];
    state.CheckBoxes.genres = ["romance"];
    let expectedState = [
      {
        id: 1,
        name: "love me like you do",
        artist: "ellie goulding",
        length: "4:09",
        genre: "romance"
      },
      {
        id: 12,
        name: "kasam se",
        artist: "ellie goulding",
        length: "4:09",
        genre: "romance"
      }
    ];
    expect(songsList(state)).toEqual(expectedState);
  });
  test("should return the correct state for value as well as genre and artist", () => {
    state.CheckBoxes.artists = ["skylar grey"];
    state.CheckBoxes.genres = ["romance"];
    state.value = "know";
    let expectedState = [
      {
        id: 2,
        name: "i know you",
        artist: "skylar grey",
        length: "4:59",
        genre: "romance"
      }
    ];
    expect(songsList(state)).toEqual(expectedState);
  });
  test("should return correct state for multiple selected checkboxes and value", () => {
    state.CheckBoxes.artists = ["lata", "pandit bhimsen joshi"];
    state.CheckBoxes.genres = ["classical", "sad"];
    state.value = "   ";
    let expectedState = [
      {
        id: 6,
        name: "akiya hari darshan",
        artist: "pandit bhimsen joshi",
        length: "7:56",
        genre: "classical"
      },
      {
        id: 7,
        name: "baaje re murila baje",
        artist: "lata",
        length: "4:59",
        genre: "sad"
      },
      {
        id: 18,
        name: "haale dil",
        artist: "pandit bhimsen joshi",
        length: "7:56",
        genre: "sad"
      }
    ];
    expect(songsList(state)).toEqual(expectedState);
  });
  test("should return an empty array as no songs match the given condition", () => {
    state.CheckBoxes.artists = ["skylar grey"];
    state.CheckBoxes.genres = ["folk"];
    let expectedState = [];
    expect(songsList(state)).toEqual(expectedState);
  });
  test("should return the correct state for one value and artist",()=>{
    state.CheckBoxes.artists = ["priyam guha","lata"];
    state.value = "a";
    let expectedState = [
      {
        id: 7,
        name: "baaje re murila baje",
        artist: "lata",
        length: "4:59",
        genre: "sad"
      },
      {
        id: 10,
        name: "hari kripa",
        artist: "priyam guha",
        length: "3:33",
        genre: "jazz"
      }
    ];
    expect(songsList(state)).toEqual(expectedState);
  })

});
