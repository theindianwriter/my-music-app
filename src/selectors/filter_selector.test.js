import filterList from "./filterbox_selector";

describe("test on filterbox selector", () => {
  let state;
  beforeEach(() => {
    state = {
      allsongs: [
        {
          id: 1,
          name: "love me like you do",
          artist: "ellie goulding",
          length: "4:09",
          genre: "pop"
        },
        {
          id: 2,
          name: "i know you",
          artist: "skylar grey",
          length: "4:59",
          genre: "romance"
        },
        {
          id: 3,
          name: "skrt on me",
          artist: "calvin harris",
          length: "3:08",
          genre: "romance"
        }
      ]
    };
  });
  afterEach(() => {
    state = {};
  });
  test("it should return the correct state", () => {
    let expectedState = {
      genres: ["pop", "romance"],
      artists: ["ellie goulding", "skylar grey", "calvin harris"]
    };
    expect(filterList(state)).toEqual(expectedState);
  });
  test("it should return the correct state if there is not songs", () => {
    state.allsongs = [];
    let expectedState = {
      genres: [],
      artists: []
    };
    expect(filterList(state)).toEqual(expectedState);
  });
});
