export  const testState = {
  allsongs: [
    {
      id: 1,
      name: "love me like you do",
      artist: "ellie goulding",
      length: "4:09",
      genre: "romance"
    },
    {
      id: 2,
      name: "i know you",
      artist: "skylar grey",
      length: "4:59",
      genre: "romance"
    },
    {
      id: 6,
      name: "akiya hari darshan",
      artist: "pandit bhimsen joshi",
      length: "7:56",
      genre: "classical"
    },
    {
      id: 7,
      name: "baaje re murila baje",
      artist: "lata",
      length: "4:59",
      genre: "sad"
    },
    {
      id: 10,
      name: "hari kripa",
      artist: "priyam guha",
      length: "3:33",
      genre: "jazz"
    },
    {
      id: 12,
      name: "kasam se",
      artist: "ellie goulding",
      length: "4:09",
      genre: "romance"
    },
    {
      id: 18,
      name: "haale dil",
      artist: "pandit bhimsen joshi",
      length: "7:56",
      genre: "sad"
    }
  ],
  value: "",
  CheckBoxes: { artists: [],genres: [] } 
};
